from argparse import ArgumentParser
import matplotlib.pyplot as plt
import os
import subprocess
import re
from itertools import repeat

def exec_profile_rust(producers, consumers, msgs, impl):
    args = [
        "./concurrent-queue-rs/target/release/concurrent-queue",
        "profile",
        "-p", str(producers),
        "-c", str(consumers),
        "-n", str(msgs),
        "-i", impl,
    ]
    print(" ".join(args))
    completed = subprocess.run(args, capture_output=True, text=True)
    return float(completed.stdout)

def exec_profile_java(producers, consumers, msgs, impl):
    args = [
        "java", "-cp", "concurrent-queue-java", "Main",
        impl,
        str(producers),
        str(consumers),
        str(msgs),
        "profile", "0", "0",
    ]
    print(" ".join(args))
    completed = subprocess.run(args, capture_output=True, text=True)
    return float(completed.stdout)

def exec_avg_profile(f, n, producers, consumers, msgs, impl):
    sum = 0.0
    for _ in range(n):
        sum += f(producers, consumers, msgs, impl)
    return sum / n

def exec_contention_rust(thread_pairs, msgs, impl):
    args = [
        "./concurrent-queue-rs/target/release/concurrent-queue",
        "contention",
        "-t", str(thread_pairs),
        "-n", str(msgs),
        "-i", impl,
    ]
    print(" ".join(args))
    completed = subprocess.run(args, capture_output=True, text=True)
    return int(completed.stdout) / 1000000000

def exec_contention_java(thread_pairs, msgs, impl):
    args = [
        "java", "-cp", "concurrent-queue-java", "Main",
        impl,
        str(thread_pairs),
        str(thread_pairs),
        str(msgs),
        "profile2", "0", "0",
    ]
    print(" ".join(args))
    completed = subprocess.run(args, capture_output=True, text=True)
    return int(completed.stdout) / 1000000000

def exec_avg_contention(f, n, thread_pairs, msgs, impl):
    sum = 0.0
    for _ in range(n):
        sum += f(thread_pairs, msgs, impl)
    return sum / n

def bar_graph(title, xlabel, ylabel, n, producers, consumers, msgs, impls_rust, impls_java, labels):
    out_f = "report/{}.png".format(title.lower().replace(" ", "_"))
    print("==== Getting data for:", title, "====")

    time = []
    for impl in impls_rust:
        time.append(exec_avg_profile(exec_profile_rust, n, producers, consumers, msgs, impl))
    for impl in impls_java:
        time.append(exec_avg_profile(exec_profile_java, n, producers, consumers, msgs, impl))
    fig, ax = plt.subplots()
    ax.bar(labels, time)
    ax.set(xlabel=xlabel, ylabel=ylabel, title=title)
    plt.xticks([i for i, _ in enumerate(labels)], labels, rotation=30)
    plt.tight_layout()
    fig.savefig(out_f)

def line_graph(title, xlabel, ylabel, n, thread_pairs, msgs, impls_rust, impls_java, labels):
    out_f = "report/{}.png".format(title.lower().replace(" ", "_"))
    print("==== Getting data for:", title, "====")

    fig, ax = plt.subplots()

    for impl in impls_rust:
        time = []
        for t in thread_pairs:
            time.append(exec_avg_contention(exec_contention_rust, n, t, msgs, impl))
        ax.plot(thread_pairs, time)
    for impl in impls_java:
        time = []
        for t in thread_pairs:
            time.append(exec_avg_contention(exec_contention_java, n, t, msgs, impl))
        ax.plot(thread_pairs, time)
    
    plt.legend(labels, loc="upper left")
    plt.xticks(thread_pairs)
    ax.set(xlabel=xlabel, ylabel=ylabel, title=title)
    ax.set_ylim(bottom=0)
    ax.grid()
    fig.savefig(out_f)
if __name__  == "__main__":
    """
    bar_graph(
        "SPSC Queue Implementation vs Nanoseconds per Message",
        "Implementation",
        "ns per message",
        50,
        producers=1,
        consumers=1,
        msgs=1000000,
        impls_rust=["concurrent", "mutex", "seg"],
        impls_java=["OurQueue", "LockingQueue", "JavaQueue"],
        labels=["Rust Michael & Scott", "Rust Mutex", "Crossbeam SegQueue", "Java Michael & Scott", "Java Mutex", "Java ConcurrentLinkedQueue"],
    )

    bar_graph(
        "MPSC Queue Implementation vs Nanoseconds per Message",
        "Implementation",
        "ns per message",
        50,
        producers=2,
        consumers=1,
        msgs=1000000,
        impls_rust=["concurrent", "mutex", "seg"],
        impls_java=["OurQueue", "LockingQueue", "JavaQueue"],
        labels=["Rust Michael & Scott", "Rust Mutex", "Crossbeam SegQueue", "Java Michael & Scott", "Java Mutex", "Java ConcurrentLinkedQueue"],
    )

    bar_graph(
        "MPMC Queue Implementation vs Nanoseconds per Message",
        "Implementation",
        "ns per message",
        50,
        producers=2,
        consumers=2,
        msgs=1000000,
        impls_rust=["concurrent", "mutex", "seg"],
        impls_java=["OurQueue", "LockingQueue", "JavaQueue"],
        labels=["Rust Michael & Scott", "Rust Mutex", "Crossbeam SegQueue", "Java Michael & Scott", "Java Mutex", "Java ConcurrentLinkedQueue"],
    )
    """
    line_graph(
        "Contention profiler performance",
        "Number of Producer and Consumer Threads",
        "Runtime (s)",
        50,
        thread_pairs=[1] + [i for i in range(2, 21, 2)],
        msgs=10000,
        impls_rust=["concurrent", "mutex", "seg"],
        impls_java=["OurQueue", "LockingQueue", "JavaQueue"],
        labels=["Rust Michael & Scott", "Rust Mutex", "Crossbeam SegQueue", "Java Michael & Scott", "Java Mutex", "Java ConcurrentLinkedQueue"],
    )
