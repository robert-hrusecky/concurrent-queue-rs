use crossbeam::queue::SegQueue;
use super::Queue;

impl<T: Send + Sync> Queue<T> for SegQueue<T> {
    fn enqueue(&self, data: T) {
        self.push(data);
    }

    fn dequeue(&self) -> Option<T> {
        self.pop().ok()
    }
}
