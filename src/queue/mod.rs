mod concurrent;
mod mutex_queue;
mod segmented;

pub use concurrent::ConcurrentQueue;
pub use mutex_queue::MutexQueue;

pub trait Queue<T>: Send + Sync {
    fn enqueue(&self, data: T);
    fn dequeue(&self) -> Option<T>;
}
