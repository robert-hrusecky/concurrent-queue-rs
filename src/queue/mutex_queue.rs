use std::sync::Mutex;
use std::collections::LinkedList;
use super::Queue;

pub struct MutexQueue<T>(Mutex<LinkedList<T>>);

impl<T> MutexQueue<T> {
    pub fn new() -> Self {
        Self(Mutex::new(LinkedList::new()))
    }
}

impl<T: Send + Sync> Queue<T> for MutexQueue<T> {
    fn enqueue(&self, data: T) {
        let mut list = self.0.lock().unwrap();
        list.push_back(data);
    }

    fn dequeue(&self) -> Option<T> {
        let mut list = self.0.lock().unwrap();
        list.pop_front()
    }
}
