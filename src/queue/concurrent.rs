use super::Queue;
use crossbeam::epoch::{self, Atomic, Owned};
use std::mem::{self, MaybeUninit};
use std::sync::atomic::Ordering;

#[derive(Debug)]
struct Node<T> {
    data: MaybeUninit<T>,
    next: Atomic<Node<T>>,
}

impl<T> Node<T> {
    fn new(data: T) -> Self {
        Self {
            data: MaybeUninit::new(data),
            next: Atomic::null(),
        }
    }

    fn dummy() -> Self {
        Self {
            data: MaybeUninit::uninit(),
            next: Atomic::null(),
        }
    }
}

pub struct ConcurrentQueue<T> {
    head: Atomic<Node<T>>,
    tail: Atomic<Node<T>>,
}

impl<T> ConcurrentQueue<T> {
    pub fn new() -> Self {
        let guard = epoch::pin();
        let dummy = Owned::new(Node::dummy()).into_shared(&guard);
        let result = Self {
            head: Atomic::null(),
            tail: Atomic::null(),
        };
        result.head.store(dummy, Ordering::SeqCst);
        result.tail.store(dummy, Ordering::SeqCst);
        result
    }
}

impl<T: Send + Sync> Queue<T> for ConcurrentQueue<T> {
    fn enqueue(&self, data: T) {
        let node = Owned::new(Node::new(data));
        let guard = epoch::pin();
        let node = node.into_shared(&guard);
        loop {
            let tail = self.tail.load(Ordering::SeqCst, &guard);
            // tail should never be null: The queue maintains a single dummy node
            let tail_ref = unsafe { tail.deref() };
            let next = tail_ref.next.load(Ordering::SeqCst, &guard);
            if self.tail.load(Ordering::SeqCst, &guard).as_raw() == tail.as_raw() {
                if next.is_null() {
                    if let Ok(_) =
                        tail_ref
                            .next
                            .compare_and_set(next, node, Ordering::SeqCst, &guard)
                    {
                        let _ = self
                            .tail
                            .compare_and_set(tail, node, Ordering::SeqCst, &guard);
                        break;
                    }
                } else {
                    let _ = self
                        .tail
                        .compare_and_set(tail, next, Ordering::SeqCst, &guard);
                }
            }
        }
    }

    fn dequeue(&self) -> Option<T> {
        let guard = epoch::pin();
        loop {
            let head = self.head.load(Ordering::SeqCst, &guard);
            let head_ref = unsafe { head.deref() };
            let tail = self.tail.load(Ordering::SeqCst, &guard);
            let next = head_ref.next.load(Ordering::SeqCst, &guard);
            if head.as_raw() == self.head.load(Ordering::SeqCst, &guard).as_raw() {
                if head.as_raw() == tail.as_raw() {
                    if next.is_null() {
                        return None;
                    }
                    let _ = self
                        .tail
                        .compare_and_set(tail, next, Ordering::SeqCst, &guard);
                } else {
                    let result = unsafe { next.deref().data.as_ptr().read() };
                    if let Ok(_) = self
                        .head
                        .compare_and_set(head, next, Ordering::SeqCst, &guard)
                    {
                        unsafe {
                            guard.defer_destroy(head);
                        }
                        return Some(result);
                    }
                }
            }
        }
    }
}

impl<T> Drop for ConcurrentQueue<T> {
    // Atomic<T> references do not automatically drop their content. We need this code to handle
    // dropping of nodes left in the queue, as well as to manually drop the MaybeUninit data field
    // of each node (excluding the dummy node).
    //
    // Safety:
    // In this function it is legal to convert values to owned values. Because this function takes
    // &mut self, The compiler will ensure that only one thread per queue can execute this code at
    // at time. The other safety condition for into_owned is that the converted reference is the
    // only reference to that object in the system. References that could end up dangling are
    // carefully cleared beforehand.
    fn drop(&mut self) {
        let guard = epoch::pin();
        // Clear the tail reference. It is no longer needed and will end up dangling if we don't
        // clear it now.
        self.tail = Atomic::null();
        // Points to dummy node. Should have an uninit data field. Dummy node is always present
        let dummy = unsafe { mem::replace(&mut self.head, Atomic::null()).into_owned() };

        // Shared reference to the current node. Starts at the first node after the dummy node.
        // Should be null or contain a valid data field
        let mut curr = dummy.next.load(Ordering::SeqCst, &guard);

        // Now we can drop the dummy.
        // Dropping now avoids a dangling reference as we iterate through the rest of the nodes.
        mem::drop(dummy);

        while !curr.is_null() {
            // We are non-null, convert to owned value
            let curr_owned = unsafe { curr.into_owned() };
            // Move curr to the next node
            curr = curr_owned.next.load(Ordering::SeqCst, &guard);
            // manually drop curr_onwed data MaybeUninit, all nodes except the dummy are
            // initialized.
            unsafe {
                mem::drop(curr_owned.data.as_ptr().read());
            }
            // curr_onwed dropped here
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use std::mem;
    use std::sync::atomic::{AtomicUsize, Ordering};

    struct DropTracker<'a> {
        count: &'a AtomicUsize,
    }

    impl<'a> DropTracker<'a> {
        fn new(count: &'a AtomicUsize) -> Self {
            Self { count }
        }
    }

    impl<'a> Drop for DropTracker<'a> {
        fn drop(&mut self) {
            self.count.fetch_add(1, Ordering::SeqCst);
        }
    }

    #[test]
    fn enqueue_basic() {
        let q = ConcurrentQueue::new();
        for i in 0..10 {
            q.enqueue(i);
        }
    }

    #[test]
    fn drop_basic() {
        let q = ConcurrentQueue::new();
        let count = AtomicUsize::new(0);
        for i in 0..10 {
            q.enqueue(DropTracker::new(&count));
        }
        mem::drop(q);
        assert_eq!(count.load(Ordering::SeqCst), 10);
    }

    #[test]
    fn dequeue_basic() {
        let q = ConcurrentQueue::new();
        for i in 0..10 {
            q.enqueue(i);
        }
        for i in 0..10 {
            if let Some(val) = q.dequeue() {
                assert_eq!(val, i);
            } else {
                panic!("Returned None too early");
            }
        }
        assert!(q.dequeue().is_none());
        assert!(q.dequeue().is_none());
    }
}
