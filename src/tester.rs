use crate::opts::TestOpts;
use crate::queue::ConcurrentQueue;
use crate::queue::Queue;
use crossbeam::thread;

pub fn run(opts: TestOpts) {
    let q = ConcurrentQueue::new();
    thread::scope(|s| {
        let handles: Vec<_> = (0..opts.consumers)
            .map(|_| {
                s.spawn(|_| {
                    let mut last_value = None;
                    let mut count = 0;
                    while count < opts.n {
                        if let Some(val) = q.dequeue() {
                            if let Some(last) = last_value {
                                if val <= last {
                                    println!("Failure on dequeue {}", count);
                                    println!("Failed with val: {}, last_value: {}", val, last);
                                    break;
                                }
                            }
                            last_value = Some(val);
                            count += 1;
                        }
                    }
                })
            })
            .collect();
        for val in 0..opts.n * opts.consumers {
            q.enqueue(val);
        }
        for handle in handles.into_iter() {
            handle.join().unwrap();
        }
    })
    .unwrap();
}
