use crate::opts::ProfileOpts;
use crate::queue::{ConcurrentQueue, MutexQueue, Queue};
use crossbeam::thread;
use crossbeam::queue::SegQueue;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::time::Instant;

fn profile(n: usize, producers: usize, consumers: usize, q: impl Queue<usize>) -> f64 {
    let start = Instant::now();
    let pending = &AtomicUsize::new(producers);
    let q = &q;
    thread::scope(|s| {
        let producer_handles: Vec<_> = (0..producers)
            .map(|id| {
                s.spawn(move |_| {
                    let size = n / producers + ((id < n % producers) as usize);
                    for i in 0..size {
                        q.enqueue(i);
                    }
                    pending.fetch_sub(1, Ordering::SeqCst);
                })
            })
            .collect();
        let consumer_handles: Vec<_> = (0..consumers)
            .map(|_| {
                s.spawn(move |_| {
                    while pending.load(Ordering::SeqCst) > 0 {
                        while let Some(_) = q.dequeue() {}
                    }
                    while let Some(_) = q.dequeue() {}
                })
            })
            .collect();
        for handle in consumer_handles
            .into_iter()
            .chain(producer_handles.into_iter())
        {
            handle.join().unwrap();
        }
    })
    .unwrap();
    let end = Instant::now();
    let duration = end - start;
    duration.as_nanos() as f64 / n as f64
}

pub fn run(opts: ProfileOpts) {
    let ns_per_message = match opts.queue.as_str() {
        "concurrent" => profile(
            opts.n,
            opts.producers,
            opts.consumers,
            ConcurrentQueue::new(),
        ),
        "mutex" => profile(opts.n, opts.producers, opts.consumers, MutexQueue::new()),
        "seg" => profile(opts.n, opts.producers, opts.consumers, SegQueue::new()),
        _ => {
            println!("Unknown queue implementation");
            return;
        }
    };
    println!("{}", ns_per_message);
}
