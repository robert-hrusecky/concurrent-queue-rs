mod queue;
mod opts;
mod profiler;
mod contention_profiler;
mod tester;

use opts::Command;

fn main() {
    match opts::parse().cmd {
        Command::Profile(opts) => profiler::run(opts),
        Command::Contention(opts) => contention_profiler::run(opts),
        Command::Test(opts) => tester::run(opts),
    }
}
