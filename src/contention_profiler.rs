use crate::opts::ContentionOpts;
use crate::queue::{ConcurrentQueue, MutexQueue, Queue};
use crossbeam::thread;
use crossbeam::queue::SegQueue;
use std::time::Instant;

fn profile(n: usize, thread_pairs: usize, q: impl Queue<usize>) -> u128 {
    let start = Instant::now();
    let q = &q;
    thread::scope(|s| {
        let producer_handles: Vec<_> = (0..thread_pairs)
            .map(|_| {
                s.spawn(move |_| {
                    for i in 0..n {
                        q.enqueue(i);
                    }
                })
            })
            .collect();
        let consumer_handles: Vec<_> = (0..thread_pairs)
            .map(|_| {
                s.spawn(move |_| {
                    let mut count = 0;
                    while count < n {
                        if let Some(_) = q.dequeue() {
                            count += 1;
                        }
                    }
                })
            })
            .collect();
        for handle in consumer_handles
            .into_iter()
            .chain(producer_handles.into_iter())
        {
            handle.join().unwrap();
        }
    })
    .unwrap();
    let end = Instant::now();
    let duration = end - start;
    duration.as_nanos()
}

pub fn run(opts: ContentionOpts) {
    let runtime = match opts.queue.as_str() {
        "concurrent" => profile(
            opts.n,
            opts.thread_pairs,
            ConcurrentQueue::new(),
        ),
        "mutex" => profile(opts.n, opts.thread_pairs, MutexQueue::new()),
        "seg" => profile(opts.n, opts.thread_pairs, SegQueue::new()),
        _ => {
            println!("Unknown queue implementation");
            return;
        }
    };
    println!("{}", runtime);
}
