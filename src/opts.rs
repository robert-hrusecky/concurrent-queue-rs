use clap::{App, AppSettings, Arg, SubCommand};

#[derive(Debug)]
pub struct Opts {
    pub cmd: Command,
}

#[derive(Debug)]
pub struct ProfileOpts {
    pub producers: usize,
    pub consumers: usize,
    pub n: usize,
    pub queue: String,
}

#[derive(Debug)]
pub struct ContentionOpts {
    pub thread_pairs: usize,
    pub n: usize,
    pub queue: String,
}

#[derive(Debug)]
pub struct TestOpts {
    pub consumers: usize,
    pub n: usize,
}

#[derive(Debug)]
pub enum Command {
    Profile(ProfileOpts),
    Contention(ContentionOpts),
    Test(TestOpts),
}

pub fn parse() -> Opts {
    let matches = App::new("Concurrent Queue")
        .version("1.0")
        .author("Robert Hrusecky and Omar Jamil")
        .about("A lock-free queue implementation")
        .setting(AppSettings::SubcommandRequired)
        .subcommand(
            SubCommand::with_name("profile")
                .about("runs the performance profiler")
                .arg(
                    Arg::with_name("producers")
                        .short("p")
                        .required(true)
                        .takes_value(true)
                        .help("number of producer threads"),
                )
                .arg(
                    Arg::with_name("consumers")
                        .short("c")
                        .required(true)
                        .takes_value(true)
                        .help("number of consumer threads"),
                )
                .arg(
                    Arg::with_name("n")
                        .short("n")
                        .required(true)
                        .takes_value(true)
                        .help("number of items each producer puts onto the queue"),
                )
                .arg(
                    Arg::with_name("impl")
                        .short("i")
                        .required(true)
                        .takes_value(true)
                        .help("number of items each producer puts onto the queue"),
                ),
        )
        .subcommand(
            SubCommand::with_name("contention")
                .about("runs the contention profiler")
                .arg(
                    Arg::with_name("thread_pairs")
                        .short("t")
                        .required(true)
                        .takes_value(true)
                        .help("number of producer/consumer threads"),
                )
                .arg(
                    Arg::with_name("n")
                        .short("n")
                        .required(true)
                        .takes_value(true)
                        .help("number of items each producer puts onto the queue"),
                )
                .arg(
                    Arg::with_name("impl")
                        .short("i")
                        .required(true)
                        .takes_value(true)
                        .help("number of items each producer puts onto the queue"),
                ),
        )
        .subcommand(
            SubCommand::with_name("test")
                .about("runs the tester")
                .arg(
                    Arg::with_name("consumers")
                        .short("c")
                        .required(true)
                        .takes_value(true)
                        .help("number of consumer threads"),
                )
                .arg(
                    Arg::with_name("n")
                        .short("n")
                        .required(true)
                        .takes_value(true)
                        .help("number of items each producer puts onto the queue"),
                ),
        )
        .get_matches();

    // You can handle information about subcommands by requesting their matches by name
    // (as below), requesting just the name used, or both at the same time
    let cmd = match matches.subcommand() {
        ("profile", Some(matches)) => Command::Profile(ProfileOpts {
            producers: matches.value_of("producers").unwrap().parse().unwrap(),
            consumers: matches.value_of("consumers").unwrap().parse().unwrap(),
            n: matches.value_of("n").unwrap().parse().unwrap(),
            queue: String::from(matches.value_of("impl").unwrap()),
        }),
        ("contention", Some(matches)) => Command::Contention(ContentionOpts {
            thread_pairs: matches.value_of("thread_pairs").unwrap().parse().unwrap(),
            n: matches.value_of("n").unwrap().parse().unwrap(),
            queue: String::from(matches.value_of("impl").unwrap()),
        }),
        ("test", Some(matches)) => Command::Test(TestOpts {
            consumers: matches.value_of("consumers").unwrap().parse().unwrap(),
            n: matches.value_of("n").unwrap().parse().unwrap(),
        }),
        _ => panic!(),
    };

    Opts { cmd }
}
