# concurrent-queue-rs

A simple Lock-free Michael-Scott queue in Rust by __Omar Jamil__ and __Robert
Hrusecky__ for CS378H Concurrency at the University of Texas at Austin. With
this project we attempt to recreate performance results from this [blog
post][post].

## Interesting links

[Our final paper](https://docs.google.com/document/d/14wdnkew1Yh4JeCp89-faq2g3l5BF7OJWhpgNuzt20gk/edit?usp=sharing)


https://doc.rust-lang.org/nomicon/

[blog post][post]

[crossbeam docs](https://docs.rs/crossbeam/0.7.3/crossbeam/)

[crossbeam](https://github.com/crossbeam-rs/crossbeam)

[Michael-Scott queue reference](https://www.cs.rochester.edu/~scott/papers/1996_PODC_queues.pdf)

[post]: http://aturon.github.io/tech/2015/08/27/epoch/
